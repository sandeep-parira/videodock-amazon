<?php
/**
 * AwsSettings.php
 *
 * @author      Nick van Ginkel <nick@videodock.com>
 * @copyright   2014 Video Dock b.v.
 *
 * This file is part of the api project.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Videodock\Component\Aws\Settings;

use Aws\Common\Aws;
use Videodock\Component\Aws\Exception\InvalidConfigurationException;

class AwsSettings
{
    /**
     * Default AWS region
     */
    const REGION_DEFAULT = 'eu-west-1';

    /**
     * @var string AWS key
     */
    protected $key;

    /**
     * @var string secret key
     */
    protected $region;

    /**
     * @var string AWS region
     */
    protected $secret;

    /**
     * @var array Additional settings
     */
    protected $settings;

    /**
     * @param string $key AWS key
     * @param string $secret AWS secret key
     * @param string $region AWS region
     * @param array $settings Additional AWS settings
     */
    public function __construct($key = null, $secret = null, $region = self::REGION_DEFAULT, array $settings = array())
    {
        $this->key    = $key;
        $this->region = $region;
        $this->secret = $secret;

        $this->settings = $settings;
    }

    /**
     * Return factory initialisation array for AWS SDK
     *
     * @return array
     */
    public function getFactoryParams()
    {
        return array(
            'key'    => $this->key,
            'secret' => $this->secret,
            'region' => $this->region
        );
    }

    /**
     * Sets factory initialisation values for AWS SDK
     */
    public function setFactoryParams($key, $secret, $region)
    {
        $this->key    = $key;
        $this->secret = $secret;
        $this->region = $region;

        return $this;
    }

    /**
     * Set Amazon secret
     *
     * @param string $secret
     *
     * @return AwsSettings
     */
    public function setSecret($secret)
    {
        $this->secret = $secret;
        return $this;
    }

    /**
     * Set Amazon key
     *
     * @param string $key
     *
     * @return AwsSettings
     */
    public function setKey($key)
    {
        $this->key = $key;
        return $this;
    }

    /**
     * Set Amazon region to use
     *
     * @param $region string
     *
     * @return AwsSettings
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * Get additional AWS settings
     *
     * @return array
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * Get AWS instance
     *
     * @return Aws
     */
    public function getAws()
    {
        $config = $this->getFactoryParams();
        $aws    = Aws::factory($config);
        return $aws;
    }
}
