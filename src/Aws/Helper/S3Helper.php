<?php
/**
 * S3Helper.php
 *
 * @author      Nick van Ginkel <nick@videodock.com>
 * @copyright   2014 Video Dock b.v.
 *
 * This file is part of the aws project.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Videodock\Component\Aws\Helper;

use Videodock\Component\Aws\Exception\AwsException;

class S3Helper
{
    static public function addSourceIpToPolicy($policy, $ip)
    {
        if(false === filter_var($ip,FILTER_VALIDATE_IP)) {
            throw new AwsException('Invalid ipaddress provided');
        }

        $ip = $ip . '/32';
        $policy = json_decode($policy);

        if (is_object($policy) && isset($policy->Statement)) {
            foreach ($policy->Statement as &$statement) {
                foreach ($statement->Condition as $key => &$condition) {
                    switch ($key) {
                        case 'IpAddress':
                            $sourceIpsKey = 'aws:SourceIp';

                            if(isset($condition->$sourceIpsKey) && !in_array($ip,$condition->$sourceIpsKey)) {
                                array_push($condition->$sourceIpsKey, $ip);
                            }

                            return json_encode($policy);

                            break;
                    }
                }
            }
        }

        throw new AwsException('Could not ipaddress to policy');
    }
}
