<?php
/**
 * AwsDataRetriever.php
 *
 * @author      Nick van Ginkel <nick@videodock.com>
 * @copyright   2014 Video Dock b.v.
 *
 * This file is part of the api project.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Videodock\Component\Aws\DataManager;

use Aws\Common\Aws;
use Aws\S3\S3Client;
use Aws\Common\Exception\MultipartUploadException;
use Aws\S3\Model\MultipartUpload\UploadBuilder;
use Videodock\Component\Aws\Exception\AwsException;
use Videodock\Component\Aws\Model\S3File;
use Videodock\Component\Aws\Settings\AwsSettings;

class S3Manager extends AwsManager
{
    /**
     * @return S3Client
     */
    protected function getS3()
    {
        return $this->aws->get('s3');
    }

    /**
     * @param $sourcePath
     * @param $bucket
     * @param $key
     * @return bool
     */
    public function multipartUpload($sourcePath, $bucket, $key)
    {
        $s3 = $this->getS3();

        $MPUploader = UploadBuilder::newInstance($sourcePath, $bucket, $key)
            ->setClient($s3)
            ->setSource($sourcePath)
            ->setBucket($bucket)
            ->setKey($key)
            ->build();

        try {
            $MPUploader->upload();
        } catch (MultipartUploadException $e) {
            $MPUploader->abort();

            return false;
        }

        return true;
    }

    /**
     * @param $bucket
     * @return string
     */
    public function getObjectPolicy($bucket)
    {
        $s3          = $this->getS3();
        $policyModel = $s3->getBucketPolicy(array('Bucket' => $bucket));
        return (string)$policyModel['Policy'];
    }

    /**
     * @param $bucket
     * @param $policyAsString
     * @return mixed
     * @throws AwsException
     */
    public function putObjectPolicy($bucket, $policyAsString)
    {
        $policyData = json_decode($policyAsString);

        if (!is_object($policyData)) {
            throw new AwsException('Invalid policy, json decode-able string expected');
        }

        return $this->getS3()->putBucketPolicy(
            array(
                'Bucket' => $bucket,
                'Policy' => $policyAsString
            )
        );
    }

    /**
     * Put object to S3 from local path
     *
     * @param $bucket
     * @param $key
     * @param $localPathName
     * @return \Guzzle\Service\Resource\Model
     */
    public function putObjectFromLocalPath($bucket, $key, $localPathName)
    {
        return $this->getS3()->putObject(
            array(
                'Bucket'     => $bucket,
                'Key'        => $key,
                'SourceFile' => $localPathName
            )
        );
    }

    /**
     * Read object from S3
     *
     * @param $bucket
     * @param $filename
     * @return null
     */
    public function getObject($bucket, $filename)
    {
        $object = array(
            'Bucket' => $bucket,
            'Key'    => $filename
        );

        try {
            $result = $this->getS3()->getObject($object);
            $body   = $result->get('Body');
            $body->rewind();

            if ($result['ContentLength'] > 0) {
                //guzzle sends out warnings for some read requests, ContentLength = 0 appears to cause this
                $content = $body->read($result['ContentLength']);
            } else {
                $content = null;
            }
        } catch (\Exception $e) {
            return null;
        }

        return $content;
    }

    public function upload($bucket, $key, $data, $acl = 'private')
    {
        $s3 = $this->getS3();
        return $s3->upload($bucket, $key, $data, $acl);
    }

    public function doesObjectExist($targetBucket, $targetFile)
    {
        $s3 = $this->getS3();
        return $s3->doesObjectExist($targetBucket, $targetFile);
    }

    /**
     * Copy an object
     *
     * @param $targetBucket
     * @param $sourceFile
     * @param $targetFile
     * @param null $sourceBucket
     * @param array $arguments
     * @return bool
     */
    public function copyObject($targetBucket, $sourceFile, $targetFile, $sourceBucket = null, array $arguments = array())
    {
        $s3 = $this->getS3();

        try {
            if (isset($sourceBucket)) {
                $copySource = $sourceBucket . '/' . $sourceFile;
            } else {
                $copySource = $targetBucket . '/' . $sourceFile;
            }

            if (!$s3->doesObjectExist($targetBucket, $targetFile)) {
                $args   = array_merge(
                    array(
                        'Bucket'     => $targetBucket,
                        'Key'        => $targetFile,
                        'CopySource' => $copySource
                    ),
                    $arguments
                );
                $result = $s3->copyObject($args);
            }
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @param string $bucket
     * @param string $prefix
     * @param int $limit
     * @return S3File[]
     */
    protected function getFilesFromBucket($bucket, $prefix = '', $limit = 500)
    {
        $listOptions = array('Bucket' => $bucket);

        if (!empty($prefix)) {
            $listOptions['Prefix'] = ltrim($prefix, '/');
        }

        $args = array('names_only' => false);
        if (isset($limit)) {
            $args['limit'] = $limit;
        }

        $iterator = $this->getS3()->getListObjectsIterator(
            $listOptions,
            $args
        );


        $files = array();
        foreach ($iterator as $i => $object) {
            if (is_string($object)) {
                $file         = $object;
                $storageClass = '';
            } else {
                $file         = $object['Key']; //Note [NvG] this happens when names_only is false
                $storageClass = $object['StorageClass'];
            }

            $resource = new S3File($bucket, basename($file), dirname($file), $storageClass);

            $files[] = $resource;
        }

        return $files;
    }

    static public function getType()
    {
        return 'aws';
    }
}