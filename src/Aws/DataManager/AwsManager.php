<?php
/**
 * AwsManager.php
 *
 * @author      Nick van Ginkel <nick@videodock.com>
 * @copyright   2014 Video Dock b.v.
 *
 * This file is part of the api project.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Videodock\Component\Aws\DataManager;

use Aws\Common\Aws;
use Videodock\Component\Aws\Settings\AwsSettings;

class AwsManager
{
    /**
     * @var \Aws\Common\Aws
     */
    protected $aws;

    /**
     * @var AwsSettings
     */
    protected $settings;

    /**
     * setSettings
     *
     * @param AwsSettings $settings
     */
    public function setSettings(AwsSettings $settings)
    {
        $this->aws      = Aws::factory($settings->getFactoryParams());
        $this->settings = $settings;
    }
}
