<?php
/**
 * EBSManager.php
 *
 * @author      Nick van Ginkel <nick@videodock.com>
 * @copyright   2014 Video Dock b.v.
 *
 * This file is part of the api project.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Videodock\Component\Aws\DataManager;

use Aws\Ec2\Ec2Client;
use Aws\ElasticBeanstalk\ElasticBeanstalkClient;
use Videodock\Component\Helpers\Helper\IpHelper;

class EbsManager extends AwsManager
{
    /**
     * @return Ec2Client
     */
    protected function getEc2()
    {
        return $this->aws->get('ec2');
    }

    /**
     * @return ElasticBeanstalkClient
     */
    protected function getElasticBeanstalk()
    {
        return $this->aws->get('elasticbeanstalk');
    }

    /**
     * Returns true if the current server has the lowest IP address from all the instances in the given EB environment
     *
     * @param $environmentName
     *
     * @throws \Exception
     *
     * @return bool
     */
    public function iAmLeader($environmentName)
    {
        $myIp = IpHelper::getMyIp();
        if (empty($myIp)) {
            throw new \Exception("Could not get my public IP address.");
        }

        $instanceList = $this->describeInstancesForEnvironment($environmentName);
        $lowestIp        = '';
        foreach ($instanceList as $instance) {
            if (isset($instance['PublicIpAddress']) && (empty($lowestIp) || $instance['PublicIpAddress'] < $lowestIp)) {
                $lowestIp = $instance['PublicIpAddress'];
            }
        }
        if (empty($lowestIp)) {
            throw new \Exception("Could not get the lowest IP for the given environment '$environmentName'.");
        }

        return ($myIp == $lowestIp);
    }

    public function describeInstancesForEnvironment($environmentName)
    {
        $ec2 = $this->getEc2();

        $args = array(
            'Filters'=> array(
                array(
                    'Name' => 'tag:Name',
                    'Values' => array($environmentName)
                )
            )
        );

        $allInstances = $ec2->describeInstances($args);

        $instances = array();

        $reservations = $allInstances->get('Reservations');

        foreach($reservations as $reservation)
        {
            foreach($reservation['Instances'] as $instance) {
                $instances[] = $instance;
            }
        }

        return $instances;
    }

    public function describeEnvironmentsForApplication($applicationName, $environmentName = null)
    {
        //$ebs = $this->getElasticBeanstalk();
        $ec2 = $this->getEc2();

        $args = array(
            'ApplicationName' => $applicationName
        );

        if(isset($environmentName)) {
            $args['EnvironmentName'] = array($environmentName);
        }

        //$model = $ebs->describeEnvironments($args);

        //$environments = ($model->get('Environments'));

//        print_r($environments);


        //$environmentId = 'e-jie46kbppm';

        $args = array(
            'Filters'=> array(
                array(
                    'Name' => 'tag:Name',
                    'Values' => array($environmentName)
                )
            )
        );
        $instances = $ec2->describeInstances($args);

        print_r($instances->get('Reservations'));

    }


}
