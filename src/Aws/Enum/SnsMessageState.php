<?php
/**
 * SnsMessageSt.php
 *
 * @author      Nick van Ginkel <nick@videodock.com>
 * @copyright   2014 Video Dock b.v.
 *
 * This file is part of the api project.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Videodock\Component\Aws\Enum;

class SnsMessageState extends Enum
{
    const ERROR       = 'ERROR';
    const PACKAGING   = 'PACKAGING';
    const PROGRESSING = 'PROGRESSING';
    const COMPLETE    = 'COMPLETED';
}
