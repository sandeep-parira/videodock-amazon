<?php
/**
 * Enum.php
 *
 * @author      Nick van Ginkel <nick@videodock.com>
 * @copyright   2014 Video Dock b.v.
 *
 * This file is part of the aws project.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Videodock\Component\Aws\Enum;

use Aws\Common\Enum as AwsEnum;

class Enum extends AwsEnum
{
    public static function getConstantForValue($value)
    {
        $key = array_search($value,static::values());
        return $key;
    }
}
