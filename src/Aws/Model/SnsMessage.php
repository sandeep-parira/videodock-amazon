<?php
/**
 * SnsMessage.php
 *
 * @author      Nick van Ginkel <nick@videodock.com>
 * @copyright   2013 Video Dock b.v.
 *
 * This file is part of the videodock/svf-api project.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Videodock\Component\Aws\Model;

use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;

use Videodock\Component\Aws\Enum\SnsType;
use Videodock\Component\Aws\Exception\InvalidSnsRequestException;

class SnsMessage
{
    const SNS_PREFIX = 'x-amz-sns-';

    const USER_AGENT = 'Amazon Simple Notification Service Agent';

    /**
     * @var ParameterBag
     */
    protected $headers;

    protected $type;

    protected $messageId;

    protected $token;

    protected $topicArn;

    protected $message;

    protected $subscribeURL;

    /**
     * @var \DateTime
     */
    protected $timestamp;

    protected $signatureVersion;

    protected $signature;

    protected $signingCertURL;

    protected $unsubscribeURL;

    /**
     * @param string $message
     *
     * @return SnsMessage
     */
    public function setMessage($message)
    {
        if (is_string($message)) {
            $message = json_decode($message);
        }
        $this->message = $message;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $messageId
     *
     * @return SnsMessage
     */
    public function setMessageId($messageId)
    {
        $this->messageId = $messageId;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessageId()
    {
        return $this->messageId;
    }

    /**
     * @param string $signature
     *
     * @return SnsMessage
     */
    public function setSignature($signature)
    {
        $this->signature = $signature;
        return $this;
    }

    /**
     * @return string
     */
    public function getSignature()
    {
        return $this->signature;
    }

    /**
     * @param string $signatureVersion
     *
     * @return SnsMessage
     */
    public function setSignatureVersion($signatureVersion)
    {
        $this->signatureVersion = $signatureVersion;
        return $this;
    }

    /**
     * @return string
     */
    public function getSignatureVersion()
    {
        return $this->signatureVersion;
    }

    /**
     * @param string $signingCertURL
     *
     * @return SnsMessage
     */
    public function setSigningCertURL($signingCertURL)
    {
        $this->signingCertURL = $signingCertURL;
        return $this;
    }

    /**
     * @return string
     */
    public function getSigningCertURL()
    {
        return $this->signingCertURL;
    }

    /**
     * @param string $subscribeURL
     *
     * @return SnsMessage
     */
    public function setSubscribeURL($subscribeURL)
    {
        $this->subscribeURL = $subscribeURL;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubscribeURL()
    {
        return $this->subscribeURL;
    }

    /**
     * @param mixed $timestamp
     *
     * @return SnsMessage
     */
    public function setTimestamp($timestamp)
    {
        if (is_string($timestamp)) {
            $timestamp = new \DateTime($timestamp);
        }
        $this->timestamp = $timestamp;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @param string $token
     *
     * @return SnsMessage
     */
    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $topicArn
     *
     * @return SnsMessage
     */
    public function setTopicArn($topicArn)
    {
        $this->topicArn = $topicArn;
        return $this;
    }

    /**
     * @return string
     */
    public function getTopicArn()
    {
        return $this->topicArn;
    }

    /**
     * @param string $type
     *
     * @throws \Videodock\Component\Aws\Exception\InvalidSnsRequestException
     * @return SnsMessage
     */
    public function setType($type)
    {
        $typeConstant = SnsType::getConstantForValue($type);

        if (false === $typeConstant) {
            throw new InvalidSnsRequestException();
        }

        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param ParameterBag $headers
     *
     * @return SnsMessage
     */
    public function setHeaders($headers)
    {
        $this->headers = $headers;
        return $this;
    }

    /**
     * @return ParameterBag
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param string $unsubscribeURL
     *
     * @return SnsMessage
     */
    public function setUnsubscribeURL($unsubscribeURL)
    {
        $this->unsubscribeURL = $unsubscribeURL;
        return $this;
    }

    /**
     * @return string
     */
    public function getUnsubscribeURL()
    {
        return $this->unsubscribeURL;
    }

    /**
     * @param Request $request
     * @throws \Videodock\Component\Aws\Exception\InvalidSnsRequestException
     * @return SnsMessage
     */
    static public function fromRequest(Request $request)
    {
        $userAgent = $request->headers->get('User-Agent');

        if ($userAgent !== self::USER_AGENT) {
            throw new InvalidSnsRequestException();
        }

        $content = $request->getContent();

        if (is_string($content)) {
            $content = json_decode($content);
        }

        $message = new SnsMessage();

        if (is_object($content)) {
            //headers
            $headers        = new ParameterBag();
            $requestHeaders = $request->headers->all();

            //recreate headers
            foreach ($requestHeaders as $key => $requestHeader) {
                if (substr($key, 0, strlen(self::SNS_PREFIX)) === self::SNS_PREFIX) {
                    $headers->set($key, array_shift($requestHeader));
                }
            }

            $message->setHeaders($headers);

            //values from content
            $properties = static::getPropertiesForClass($message);

            foreach ($properties as $property) {
                $name   = ucfirst($property); // [NvG] isn't name the same property?
                $setter = static::getSetterFromProperty($property);

                if (isset($content->$name)) {
                    try {
                        $message->$setter($content->$name);
                    } catch (\Exception $e) {
                        throw new InvalidSnsRequestException();
                    }
                }
            }

            return $message;
        }
        throw new InvalidSnsRequestException();
    }

    static public function getPropertiesForClass($class, $filter = \ReflectionProperty::IS_PROTECTED)
    {
        $reflect = new \ReflectionClass($class);
        $props   = $reflect->getProperties($filter);

        $names = array();
        foreach ($props as $prop) {
            $names[] = $prop->getName();
        }
        return $names;
    }

    /**
     * Convert an array / request key to a setter, e.g. name > setName, custom_id > setCustomId
     *
     * @param $key
     * @return string
     */
    static public function getSetterFromProperty($key)
    {
        return 'set' . (str_replace(' ', '', ucwords(str_replace('_', ' ', $key))));
    }
}
