<?php
/**
 * S3File.php
 *
 * @author      Nick van Ginkel <nick@videodock.com>
 * @copyright   2014 Video Dock b.v.
 *
 * This file is part of the api project.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Videodock\Component\Aws\Model;

class S3File
{
    /**
     * @var string
     */
    protected $bucket;

    /**
     * @var string
     */
    protected $file;

    /**
     * @var string
     */
    protected $path;

    /**
     * @var string
     */
    protected $storageClass;

    public function __construct($bucket, $file, $path = '', $storageClass = '')
    {
        $this->bucket = $bucket;
        $this->setFile($file);
        $this->setPath($path);
        $this->setStorageClass($storageClass);
    }

    /**
     * @param string $file
     *
     * @return S3File
     */
    public function setFile($file)
    {
        $this->file = $file;
        return $this;
    }

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param string $path
     *
     * @return S3File
     */
    public function setPath($path)
    {
        //cleanup path
        if(in_array($path,array('.','./'))) {
            $path = '';
        }

        $this->path = $path;
        return $this;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $storageClass
     *
     * @return S3File
     */
    public function setStorageClass($storageClass)
    {
        $this->storageClass = $storageClass;
        return $this;
    }

    /**
     * @return string
     */
    public function getStorageClass()
    {
        return $this->storageClass;
    }

    public function getFullPath()
    {
        $chunks = array_filter(array($this->getPath(),$this->getFile())); //this will make sure an empty value won't be part of the fullPath
        return implode('/',$chunks);
    }
}
