<?php
/**
 * AwsFactory.php
 *
 * @author      Nick van Ginkel <nick@videodock.com>
 * @copyright   2014 Video Dock b.v.
 *
 * This file is part of the api project.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Videodock\Component\Aws\Factory;

use Videodock\Component\Aws\Exception\InvalidConfigurationException;
use Videodock\Component\Aws\Settings\AwsSettings;

class AwsFactory
{
    /**
     * Factory method to create AWS settings, expects an associative array with key values
     *
     * @param array $settings
     * @return AwsSettings
     * @throws \Videodock\Component\Aws\Exception\InvalidConfigurationException
     */
    public function get(array $settings)
    {
        /**
         * Todo [NvG]: Could do this with reflection for protected properties
         */
        $required = array('key','secret','region');

        foreach($required as $requiredKey)
        {
            if(!isset($settings[$requiredKey])) {
                throw new InvalidConfigurationException();
            }
        }

        $additionalSettings = $settings;
        unset($additionalSettings['key']);
        unset($additionalSettings['secret']);
        unset($additionalSettings['region']);

        return new AwsSettings($settings['key'],$settings['secret'],$settings['region'], $additionalSettings);
    }
}
